FROM bellsoft/liberica-openjdk-alpine:17

ARG PORT
ENV SERVER_PORT=$PORT

COPY target/*.jar /app/target/

RUN addgroup -S practice && adduser -S practice -G practice && \
	chown -R practice:practice /app && \
	chmod ug+rwx /app/target/*jar

USER practice

WORKDIR /app

EXPOSE ${PORT}

ENTRYPOINT export JAR_FILE=`basename /app/target/*.jar` && java -jar ${PROFILE} target/${JAR_FILE}