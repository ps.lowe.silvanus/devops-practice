#!/bin/bash

set -Euo pipefail

## How To Run Container
#Run the startup.sh script in the project root directory,
# grant execution permission to the startup.sh file with 

# Run the script:

# `./startup.sh -p 8090 -s 8090 -d h2`

# where:
# - `-d` flag can either be `h2` or `mysql` to select which database you want to run with
# - `-s` flag to specify any server port number you want the app to run on in the container
# - `-p` flag to specify the port on localhost to map to the container

while getopts p:d:s: flag
do
    case "${flag}" in
        p) PORT=${OPTARG};;
        d) DATABASE=${OPTARG};;
        s) SERVER_PORT=${OPTARG};;
        *) echo "usage: $0 [-v] [-r]" >&2
            exit 1 ;;
    esac
done

LAST_COMMIT_ID=$(git rev-parse --short=8 HEAD)
LAST_COMMIT_DATE=$(git log -1 --pretty='format:%ad' --date=format:%y%m%d)

docker build --build-arg PORT=${PORT} -t lowesilvan/assignment:"${LAST_COMMIT_DATE}-${LAST_COMMIT_ID}" .

docker push lowesilvan/assignment:"${LAST_COMMIT_DATE}-${LAST_COMMIT_ID}"

if [[ "${DATABASE}" == "h2" ]]; then
	echo "::Variable set::"
	echo "DATABASE: ${DATABASE}"
	echo "SERVER_PORT: ${SERVER_PORT}}"
	echo "PORT: ${PORT}}"
	echo "LAST COMMIT ID: ${LAST_COMMIT_ID}"
	echo "LAST COMMIT DATE: ${LAST_COMMIT_DATE}"
	
	docker run -dp ${PORT}:${SERVER_PORT} -e SERVER_PORT=${SERVER_PORT} -e PROFILE=-Dspring.profiles.active=h2 --name api lowesilvan/assignment:"${LAST_COMMIT_DATE}-${LAST_COMMIT_ID}" 
	echo "api container created and available on localhost at ${PORT}"
	
	elif [[ "${DATABASE}" == "mysql" ]]; then
	echo "::Variable set::"
	echo "DATABASE: ${DATABASE}"
	echo "SERVER_PORT: ${SERVER_PORT}}"
	echo "PORT: ${PORT}}"
	echo "LAST COMMIT ID: ${LAST_COMMIT_ID}"
	echo "LAST COMMIT DATE: ${LAST_COMMIT_DATE}"
	
	export TAG="${LAST_COMMIT_DATE}-${LAST_COMMIT_ID}" 
	export SERVER_PORT="${SERVER_PORT}"
	export PORT=${PORT}
	docker compose up -d --force-recreate
	
	echo "mysql container created"
	echo "api container created and available on localhost at ${PORT}"
		
else 
echo "Database flag does not exist. Specify either -d h2 or -d mysql"
fi		
echo "script done"